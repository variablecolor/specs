Pod::Spec.new do |s|  
  s.name              = 'VariableColor'
  s.version           = '8.7.0'
  s.summary           = 'Framework for working with Variable color devices.'
  s.homepage          = 'https://variableinc.com/'

  s.author            = { 'Name' => 'dev@variableinc.com' }
  s.license           = { :type => 'closed', :text => 'This project is available for use only under signed agreement with Variable, Inc.' }

  s.platform          = :ios
  s.source            = { :http => 'https://bitbucket.org/variablecolor/specs/downloads/VariableColor.framework-8.7.0.zip' }
  s.public_header_files = "VariableColor.framework/Headers/*.h"
  s.source_files = "VariableColor.framework/Headers/*.h"
  s.vendored_frameworks = "VariableColor.framework"
  s.platform = :ios
  s.swift_version = "4.2"
  s.ios.deployment_target  = '11.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end